﻿﻿using System;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Mvc;
using Z01.GraphQLScripts;

namespace Z01.Controllers
{
    [Route("[controller]")]
    public class GraphController : Controller
    {
        private ISchema schema;
        private IDocumentExecuter documentExecuter;
        public GraphController(ISchema schema, IDocumentExecuter documentExecuter)
        {
            this.schema = schema;
            this.documentExecuter = documentExecuter;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            if(query==null) 
                throw new ArgumentNullException("No query sent!");

            var inputs = query.Variables?.ToInputs();
            var executionOptions = new ExecutionOptions
            {
                Schema = schema,
                Query = query.Query,
                Inputs = inputs
            };

            var result = await documentExecuter.ExecuteAsync(executionOptions);

            if (result.Errors?.Count>0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
    }
}