﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
 using System.Net.Mime;
 using System.Text;
using System.Threading.Tasks;
 using Microsoft.AspNetCore.Cors;
 using Microsoft.AspNetCore.Http;
 using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Z01.GraphQLScripts;
using Z01.Models;
using Z01.Models.ViewModels;
using Z01.Utility;
using Z01.Utility.DataManager;

namespace Z01.Controllers
{
    [ApiController]
    [EnableCors("MyPolicy")]
    [Route("[controller]")]
    public class NotesController : ControllerBase
    {
        private int notesPerPage = 4; 
        //private readonly IDataManager<Note> notes = new NotesFilesManager();
        private readonly IDataManager<Note> notes = new NotesDatabaseDataManger();

        [HttpGet]
        [Route("notes")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetNotes(string category, string startDate, string endDate, int page)
        {
            IEnumerable<Note> allNotes = notes.FindAll().ToList();
            IEnumerable<Note> filteredNotes;
            IEnumerable<string> categories = GetAllCategories(allNotes);
            DateTime from = DateTime.MinValue;
            if (!string.IsNullOrEmpty(startDate))
            {
                from = DateTime.Parse(startDate);
            }
            DateTime to = DateTime.MaxValue;
            if (!string.IsNullOrEmpty(endDate))
            {
                to = DateTime.Parse(endDate);
            }
            Filter filter = new Filter(from, to, category);
            filteredNotes = filter.Apply(allNotes);
            filteredNotes = ApplyPagination(filteredNotes, page-1, out int currentPage, out int pagesCount);
            return Ok(new
                {notes = filteredNotes, categories = categories, currentPage = currentPage, pagesCount = pagesCount});
        }

        private IEnumerable<string> GetAllCategories(IEnumerable<Note> allNotes)
        {
            List<string> categories = new List<string>();
            foreach (Note n in allNotes)
            {
                foreach (string category in n.Categories)
                {
                    if (!categories.Contains(category))
                    {
                        categories.Add(category);
                    }
                }
            }
            categories.Sort((x, y) => x.CompareTo(y));
            return categories;
        }

        private IEnumerable<Note> ApplyPagination(IEnumerable<Note> notes, int page, out int currentPage, out int pagesCount)
        {
            List<Note> notesList = notes.ToList();
            int count = notesList.Count;
            pagesCount = (int)Math.Ceiling((float)count / notesPerPage);
            currentPage = page;

            currentPage = Math.Max(Math.Min(currentPage, pagesCount-1), 0);
            int notesOnPage = Math.Min(count - currentPage * notesPerPage, notesPerPage);
            if (pagesCount == 0)
            {
                pagesCount = 1;
            }
            var paginatedNotes = notesList.Skip(currentPage * notesPerPage).Take(notesOnPage);
            currentPage += 1;
            return paginatedNotes;
        }
        
        [HttpGet("{title}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetNote(string title)
        {
            Note note = notes.Find(t => t.Title == title);
            if (note == null)
            {
                return StatusCode(404, "Note does not exist. It could be deleted by someone else.");
            }
            return Ok(new {note = note});
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult DeleteNote(int id)
        {
            var noteToDelete = notes.Find(t => t.Id == id);
            if (noteToDelete != null)
            {
                notes.Remove(noteToDelete);
            }
            return Ok();
        }

        [HttpPost]
        [Route("add")]
        //[Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddNote([FromBody] Note note)
        {
            try
            {
                notes.Add(note);
            }
            catch (DbUpdateException e)
            {
                return StatusCode(500, e.InnerException.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            return Ok();
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult UpdateNote([FromBody] Note note)
        {
            try
            {
                notes.Update(note); //Throw concurency error
            }
            catch (NotepadUpdateException e)
            {
                return StatusCode(500, e.Message);
            }
            return Ok();
        }
    }
}