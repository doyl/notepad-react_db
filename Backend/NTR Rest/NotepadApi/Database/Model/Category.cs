﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Z01.Database.Model
{
    public class Category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryID { get; set; }
        public string Title { get; set; }
        public ICollection<NoteCategory> NotesCategories {get; set; }
    }
}