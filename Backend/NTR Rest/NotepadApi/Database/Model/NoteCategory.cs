﻿using Z01.Models;

namespace Z01.Database.Model
{
    public class NoteCategory
    {
        public int NoteID { get; set; }
        public Note Note { get; set; }
        
        public int CategoryID { get; set; }
        public Category Category { get; set; }
    }
}