﻿using System;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Z01.Database.Model;

namespace Z01.Database
{
    public class NotepadDbContext : DbContext
    {
        public DbSet<Note> Note { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<NoteCategory> NoteCategory { get; set; }
        
        public NotepadDbContext(DbContextOptions<NotepadDbContext> options) : base(options){}

        public NotepadDbContext() : base()
        {
            Console.Write(Database.GetDbConnection().ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("dygas");
            modelBuilder.Entity<Note>().HasKey(n => n.NoteID);
            modelBuilder.Entity<Note>().Property(n => n.NoteID).HasColumnName("IDNote");
            modelBuilder.Entity<Note>().Property(n => n.NoteDate).HasColumnName("Date").HasColumnType("datetime").IsRequired();
            modelBuilder.Entity<Note>().Property(n => n.Description).HasMaxLength(1024).IsFixedLength();
            modelBuilder.Entity<Note>().Property(n => n.Timestamp).IsConcurrencyToken(); 
            modelBuilder.Entity<Note>().Property(n => n.Title).IsRequired().HasMaxLength(64);

            modelBuilder.Entity<Category>().HasKey(c => c.CategoryID);
            modelBuilder.Entity<Category>().Property(c => c.CategoryID).HasColumnName("IDCategory");
            modelBuilder.Entity<Category>().Property(c => c.Title).HasColumnName("Name").IsRequired().HasMaxLength(32);

            modelBuilder.Entity<NoteCategory>().HasKey(nc => new {nc.NoteID, nc.CategoryID});
            modelBuilder.Entity<NoteCategory>().Property(nc => nc.CategoryID).HasColumnName("IDCategory");
            modelBuilder.Entity<NoteCategory>().Property(nc => nc.NoteID).HasColumnName("IDNote");
            
            modelBuilder.Entity<NoteCategory>()
                .HasOne<Note>(nc => nc.Note)
                .WithMany(n => n.NotesCategories)
                .HasForeignKey(nc => nc.NoteID)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NoteCategory_Note");;

            modelBuilder.Entity<NoteCategory>()
                .HasOne<Category>(nc => nc.Category)
                .WithMany(c => c.NotesCategories)
                .HasForeignKey(nc => nc.CategoryID)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NoteCategory_Category");
            
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Server=localhost,8200;Database=NTR2019Z;User Id=dygas; Password=283730;");
            optionsBuilder.UseSqlServer (
                @"Server=base.ii.pw.edu.pl,1433;Database=NTR2019Z;User Id=User2019Z;Password=Password2019Z;");
        }
    }
}