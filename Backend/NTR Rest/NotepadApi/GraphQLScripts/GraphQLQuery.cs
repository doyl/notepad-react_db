﻿﻿using Newtonsoft.Json.Linq;

namespace Z01.GraphQLScripts
{
    public class GraphQLQuery
    {
        public string OperationName { get; set; }
        public string Query { get; set; }
        public JObject Variables { get; set; }
    }
}