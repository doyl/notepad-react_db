﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Z01.Models;
using System.Linq;
using System.Text;

namespace Z01.Utility.DataManager
{
    public class NotesFilesManager : IDataManager<Note>
    {
        private string basePath = "./Data/";
        
        public void Add(Note item)
        {
            StringBuilder path = new StringBuilder();
            path.Append(basePath).Append(item.Title).Append(item.IsMarkdown ? ".md" : ".txt");
            string fileContent = BuildNoteFile(item);
            File.WriteAllText(path.ToString(), fileContent);
        }

        private string BuildNoteFile(Note item)
        {
            StringBuilder fileContent = new StringBuilder();
            fileContent.Append("Categories: ");
            foreach (string cat in item.Categories)
            {
                fileContent.Append(cat);
                fileContent.Append(", ");
            }
            if(item.Categories.Any())
                fileContent.Remove(fileContent.Length - 2, 2);
            fileContent.Append("\nDate: ").Append(item.Date.ToString("yyyy/MM/dd"));//.Replace('-', '/'));
            fileContent.Append("\nContent: " ).Append(item.Content);
            return fileContent.ToString();
        }

        public void Remove(Note item)
        {
            string[] files = Directory.GetFiles(basePath);
            if (files.Contains(basePath + item.Title + ".txt"))
            {
                File.Delete(basePath + item.Title + ".txt");
            }
            else if (files.Contains(basePath + item.Title + ".md"))
            {
                File.Delete(basePath + item.Title + ".md");
            }
        }

        public void Update(Note item)
        {
            throw new NotImplementedException();
        }

        public Note Find(Func<Note, bool> checkingFunction)
        {
            IEnumerable<Note> parsedNotes = FindAll();
            foreach (Note n in parsedNotes)
            {
                if (checkingFunction(n))
                    return n;
            }

            return null;
        }

        public IEnumerable<Note> FindAll()
        {
            string[] files = Directory.GetFiles(basePath);
            return files.Select(ParseNote);
        }

        public IEnumerable<Note> FindAll(Func<Note, bool> checkingFunction)
        {
            throw new NotImplementedException();
        }

        private Note ParseNote(string file)
        {
            StreamReader reader = new StreamReader(file);
            string title = ParseTitle(file);
            IEnumerable<string> categories = ParseCategories(reader.ReadLine());
            DateTime date = ParseDate(reader.ReadLine());
            string content = ParseContent(reader.ReadToEnd());
            bool isMarkdown = file.Substring(file.LastIndexOf('.')) == ".md";
            reader.Close();
            return new Note(title, content, date, categories, isMarkdown);
        }

        private IEnumerable<string> ParseCategories(string readLine)
        {
            string categoriesList = readLine.Remove(0, "Categories: ".Length);
            List<string> categories = new List<string>();
            while (categoriesList.Length != 0)
            {
                int commaIndex;
                if (categoriesList.Contains(','))
                    commaIndex = categoriesList.IndexOf(',');
                else
                    commaIndex = categoriesList.Length;
                categories.Add(categoriesList.Substring(0, commaIndex));
                categoriesList = categoriesList.Remove(0, Math.Min(commaIndex+2, categoriesList.Length));
            }
            return categories;
        }

        private string ParseTitle(string toParse)
        {
            int indexOfSlash = toParse.LastIndexOf('/')+1;
            int indexOfDot = toParse.LastIndexOf('.');
            return toParse.Substring(indexOfSlash, indexOfDot - indexOfSlash);
        }

        private string ParseContent(string toParse)
        {
            return toParse.Remove(0, "Content: ".Length);
        }

        private DateTime ParseDate(string toParse)
        {
            string pureDate = toParse.Remove(0, "Date: ".Length);
            return DateTime.ParseExact(pureDate.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
        }
    }
}