﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Z01.Utility
{
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : 
                JsonConvert.DeserializeObject<T>(value);
        }

        public static bool TryGet<T>(this ISession session, string key, out T value)
        {
            string json = session.GetString(key);
            if (json == null)
            {
                value = default(T);
                return false;
            }
            else
            {
                value = JsonConvert.DeserializeObject<T>(json);
                return true;
            }
        }

        public static bool ContainsAny(this string me, params char[] chars)
        {
            foreach (char c in chars)
            {
                if (me.Contains(c.ToString()))
                    return true;
            }

            return false;
        }
    }
}