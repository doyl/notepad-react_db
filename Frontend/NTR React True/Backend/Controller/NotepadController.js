let moment = require('moment');

let NoteFileManager = require('../DataMangers/NoteFileManager');
let Note = require('../Models/Note');
const paginate = require('jw-paginate');

const notesPerPage = 5;

exports.findAllNotes = (req, res) => {
    console.log('You wish to find all, your wish will not fall');
    const noteFileManager = new NoteFileManager();
    const {notes, categories} = noteFileManager.findAll();
    let filtered = notes;
    const startDate = moment(req.query.startDate, "YYYY-MM-DD");
    const endDate = moment(req.query.endDate, "YYYY-MM-DD");

    if(startDate.isValid()) {
        console.log('Start Date: ' + req.query.startDate);
        filtered = filtered.filter(note => isAfterOrSame(note.date, startDate))
    }
    if(endDate.isValid()) {
        console.log('End Date: ' + req.query.endDate);
        filtered = filtered.filter(note => isBeforeOrSame(note.date, endDate))
    }
    if(req.query.filterCategory !== undefined && req.query.filterCategory !== "(All)") {
        console.log('Filter category: ' + req.query.filterCategory);
        filtered = filtered.filter(note => noteHasCategory(note, req.query.filterCategory))

        console.log("After filtring: " + filtered);
    }
    console.log("page: " + req.query.page);
    let currentPage = req.query.page;
    const pager = paginate(filtered.length, currentPage, notesPerPage);
    filtered = filtered.splice(pager.startIndex, notesPerPage);

    res.send({
        categories: categories,
        notes: filtered,
        currentPage: pager.currentPage,
        pagesCount: pager.totalPages
    })
};

function noteHasCategory(note, category) {
    const foundNote = note.categories.find(cat => {
        return cat === category});
    return foundNote;
}

function isAfterOrSame(date1, date2) {
    date1 = moment(date1, "YYYY-MM-DD");
    console.log(date1);
    return date1.isSameOrAfter(date2);
}

function isBeforeOrSame(date1, date2) {
    date1 = moment(date1, "YYYY-MM-DD");
    console.log(date1);
    return date1.isSameOrBefore(date2);
}

exports.findNote = (req, res) => {
    const noteFileManager = new NoteFileManager();
    const title = req.params.title;
    let note;
    try{
        note = noteFileManager.find(title);
    } catch(error) {
        return res.send(error.message);
    }

    res.send({
        note: note
    })
};

exports.create = (req, res) => {
    const noteFileManager = new NoteFileManager();
    const markdown = req.body.markdown;
    console.log("Markdown is " + markdown);
    const note = new Note(req.body.title, req.body.content,req.body.date, req.body.markdown, req.body.categories);
    try{
        noteFileManager.create(note);
    } catch (e) {
        return res.send(e.message);
    }
    res.send('Created');
};

exports.update = (req, res) => {
    const noteFileManager = new NoteFileManager();
    const note = new Note(req.body.title, req.body.content,req.body.date, req.body.markdown, req.body.categories);
    try{
        noteFileManager.update(req.params.old_title, note); //why no old title?
    } catch (e) {
        return res.send(e.message);
    }
    res.send('Updated');
};

exports.delete = (req, res) => {
    const noteFileManager = new NoteFileManager();
    noteFileManager.delete(req.params.title);
    res.send("Deleted");
};

exports.test = (req, res) => {
    console.log("Hello world should appear just right");
    res.send("Hello world");
};