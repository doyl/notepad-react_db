const path = require('path');
const notesDirectory = path.join(__dirname, '../Data/Notes');
const fs = require('fs');
const moment = require('moment');

let Note = require('../Models/Note');

module.exports = class NoteFileManager {
    create(note) {
        if(this.find(note.title) !== undefined){
            throw Error('Note with title '+ note.title + " already exists!");
        }
        const extension = note.isMarkdown ? 'md' : 'txt';
        let content = 'category: ';
        note.categories.forEach(cat => {
            content += cat;
            content += ', ';
        });
        content += '\ndate: ';
        content += moment(note.date).format('YYYY/MM/DD');
        content += '\ncontent:\n';
        content += note.content;
        fs.writeFile(`${notesDirectory}/${note.title}.${extension}`, content, err => {
            if(err) throw err;
        })
    }

    delete(title) {
        console.log("deletin note: " + title);
        const file = this.find(title);
        console.log("file detected: " + file);
        if(file !== undefined) {
            const extension = file.isMarkdown ? '.md' : '.txt';
            fs.unlinkSync(`${notesDirectory}/${file.title}${extension}`)
        }
    }

    find(title) {
        if(fs.existsSync(`${notesDirectory}/${title}.txt`))
            return this.fileToNote(title + '.txt');
        else if (fs.existsSync(`${notesDirectory}/${title}.md`))
            return this.fileToNote(title + '.md');
        return undefined;
    }

    findAll() {
        const files = fs.readdirSync(notesDirectory);
        const notes = files.map(file => {
            return this.fileToNote(file);
        });
        const allCategories = [];
        notes.forEach(note => {
            note.categories.forEach(cat => {
                allCategories.push(cat);
            })
        });
        return {
            notes: notes,
            categories: allCategories
        }
    }

    update(oldTitle, note) {
        if(oldTitle !== note.title && this.find(note.title)!==undefined) {
            throw Error('Title '+ note.title + " is already used!");
        }
        this.delete(oldTitle);
        this.create(note);
    }

    fileToNote(file){
        const filePath = `${notesDirectory}/${file}`;
        const [title, extension] = file.split('.');
        const fileContent = fs.readFileSync(filePath, 'utf-8');
        const isMarkdown = (extension === 'md');
        const lines = fileContent.split('\n');
        const categories = this.parseCategories(lines[0]);
        const date = moment(new Date(lines[1].split(':')[1].trim())).format('YYYY-MM-DD');
        const content = fileContent.split('content:\n')[1];
        const note =  new Note(title, content, date, isMarkdown, categories);
        return note;
    }

    parseCategories(line){
        const categories = [];
        line.split(':')[1].split(',').forEach(cat => {
            cat = cat.trim();
            if(cat.length===0) return;
            categories.push(cat);
        });
        return categories;
    }
};
