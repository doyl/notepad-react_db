module.exports = class Note {
    constructor(title, content, date, isMarkdown, categories) {
        this.title = title;
        this.date = date;
        this.content = content;
        this.isMarkdown = isMarkdown;
        this.categories = categories;
    }
}