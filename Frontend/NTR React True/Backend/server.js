const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const notepadController = require('./Controller/NotepadController');

const port = 8080;
const app = express();

const router = express.Router();
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

router.get('/test', notepadController.test);
router.get('/', notepadController.findAllNotes);
router.get('/notes/:title', notepadController.findNote);
router.post('/add_note', notepadController.create);
router.delete('/delete_note/:title', notepadController.delete);
router.put('/update_note/:old_title', notepadController.update);

app.use('/notepad_api', router);


app.listen(port, () => {
    console.log(`Server is listening at port ${port}`);
})