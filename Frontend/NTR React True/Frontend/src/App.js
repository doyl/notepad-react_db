import React from 'react';
import NotesList from './Components/NotesList'
import NoteEdit from './Components/NoteEdit'
import {BrowserRouter as Router, Route} from 'react-router-dom'

function App() {
  return (
    <Router>
      <Route exact path='/' component={NotesList}/>
      <Route exact path={'/edit/:title'} component={NoteEdit}/>
      <Route exact path={'/edit'} component={NoteEdit}/>
    </Router>
  );
}

export default App;
