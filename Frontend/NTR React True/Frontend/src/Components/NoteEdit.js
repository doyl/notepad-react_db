import React, {Component} from 'react'
import Axios from "axios";
import {Link} from "react-router-dom";
import moment from "moment";
import {BACKEND, GET_NOTES} from "../Constants";

class NoteEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.match.params.title || '',
            content: '',
            date: moment().format('YYYY-MM-DD'),
            markdown: false,
            categories: [],
            newCategory: '',
            oldTitle: '',
            timestamp: '',
            id: 0
        };
    }

    componentDidMount() {
        if (this.state.title !== '') {
            Axios.get(`${BACKEND}/${this.state.title}`).then(res => {

                this.setState({
                    date: moment(res.data.note.date).format('YYYY-MM-DD'),
                    categories: res.data.note.categories.map(cat => {return {name: cat}}),
                    markdown: res.data.note.isMarkdown,
                    content: res.data.note.content,
                    oldTitle: res.data.note.title,
                    timestamp: res.data.note.timestamp,
                    id: res.data.note.id
                });

            }).catch(err => {
                alert(err);
            })
        }
    }

    setTitle = (event) => {
        this.setState({title: event.target.value})
    };

    setContent = (event) => {
        this.setState({content: event.target.value})
    };

    setDate = (event) => {
        this.setState({date: event.target.value})
    };

    setMarkdown = (event) => {
        //alert(event.target.value);
        this.setState({markdown: !this.state.markdown})
    };

    setCategory = (event) => {
        this.setState({newCategory: event.target.value})
    };

    addCategory = () => {
    const categories = this.state.categories;
    if(this.state.newCategory === '')
    {
        alert("Category can't be empty!");
        return;
    }
    //alert(categories);
    //alert(this.state.newCategory);
    if(categories.map(x => x.name).indexOf(this.state.newCategory)!==-1)
    {
        alert("Category " + this.state.newCategory + " is already added!");
        return;
    }
    categories.push({name: this.state.newCategory});
    this.setState({categories: categories});
    this.setState({newCategory: ""})
    };

    removeCategory = () => {
        let categories = this.state.categories;
        categories = categories.filter(cat => cat.name !== this.state.newCategory);
        this.setState({categories: categories});
    };

    addNote = () => {
        //alert(this.state.date);
        if(!moment(this.state.date).isValid()) {
            alert("Date is invalid!");
            return;
        }
        if(this.state.title === ''){
            alert("Title can't be empty!");
            return;
        }
        if(this.state.oldTitle !== "") {
            this.updateNote();
        } else {
            this.addNewNote();
        }
    };

    addNewNote = () => {
        //const md = this.state.markdown ? 1 : 0;
        Axios.post(`${BACKEND}/add`, {
            Title: this.state.title,
            Date: this.state.date,
            IsMarkdown: this.state.markdown,
            Content: this.state.content,
            Categories: this.state.categories.map(cat => cat.name)
        }).then(res => {
            if(res.status !== 200){
                alert("There was an error: " + res.data)
            } else {
                this.props.history.push('/');
            }
        }).catch(err => {
            alert("Error caught: " + err + err.response.data);
        })
    };

    updateNote = () => {
        //const md = this.state.markdown ? 1 : 0; //Tutaj
        Axios.put(`${BACKEND}`, {
            title: this.state.title,
            date: this.state.date,
            IsMarkdown: this.state.markdown,
            content: this.state.content,
            categories: this.state.categories.map(cat => cat.name),
            timestamp: this.state.timestamp,
            id: this.state.id
        }).then(res => {
            if(res.status === 200){
                this.props.history.push('/');
            } else {
                alert(res.data);
            }
        }).catch(err => {
            alert(err.response.data);
        })
    };

    cancel = () => {

    };

    render() {
        return (
            <div className = "container mt-3">
                <div className="row ml-1">
                    <span>Title</span>
                    <input className = "ml-2" value={this.state.title} onChange={this.setTitle}/>
                </div>
                <div className="row-cols-2 mt-3 md-3">
                    <span className="mr-2">Date</span>
                    <input type={"date"} value={this.state.date} onChange={this.setDate}/>
                    <span className={"ml-5"}>Is markdown</span>
                    <input className={"mt-2 ml-2"} checked={this.state.markdown} value = {!this.state.markdown} onChange={this.setMarkdown} type="checkbox"/>
                </div>
                <label className="row mt-3 ml-2">Content</label>
                <div className={"row-cols-1 mt-1"}>
                    <textarea className = "col ml-2" value = {this.state.content} onChange={this.setContent}/>
                </div>
                <div className="row mt-2 ml-2">
                    <div className="col">
                        <span>Categories</span>
                        <ul className="list-group ml-3">
                            {this.state.categories.map(cat => {
                                return (
                                    <div className="row mt-1">
                                        <li className={"list-group-item"} key={cat.name}>{cat.name}</li>
                                    </div>
                                )
                            })}
                        </ul>
                    </div>
                <div>
                    <span>Category</span>
                    <input value={this.state.newCategory} onChange={this.setCategory}/>
                    <button type="button" className="btn btn-outline-success ml-2" onClick={this.addCategory}>Add</button>
                    <button type="button" className="btn btn-danger ml-2" onClick={this.removeCategory}>Remove</button>
                </div>
                </div>
                <button type="button" className="btn btn-outline-success mt-3" onClick={this.addNote}>Save</button>
                <Link to={"/"}>
                    <button type="button" className="btn btn-danger ml-2 mt-3" onClick={this.cancel}>Cancel</button>
                </Link>
            </div>
        );
    }
}

export default NoteEdit
