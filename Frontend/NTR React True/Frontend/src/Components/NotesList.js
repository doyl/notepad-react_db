import React, {Component} from 'react'
import Axios from "axios";
import {Link} from "react-router-dom";
import {BACKEND, GET_NOTES} from "../Constants";
import moment from "moment";

class NotesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: [],
            categories: [],
            startDate: '',
            endDate: '',
            filterCategory: '(All)',
            currentPage: 1,
            pagesCount: 1
        };
    }

    componentDidMount() {
        let startDate = sessionStorage.getItem('startDate');
        if(startDate==null)
            startDate = '';
        let endDate = sessionStorage.getItem('endDate');
        if(endDate == null)
            endDate = '';
        let filterCategory = sessionStorage.getItem('filterCategory');
        if(filterCategory==null)
        {
            filterCategory = '(All)';
        }
        let page = sessionStorage.getItem('page');
        if(page==null)
        {
            page = 1 - 1 + 1;
        }
        this.setState({startDate: startDate, endDate: endDate, filterCategory: filterCategory, currentPage: page}, this.loadNotes);
    }

    setStartDate = (event) => {
        if(event.target.value !== undefined) {
            this.setState({startDate: event.target.value})
        }
    };

    setEndDate = (event) => {
        if(event.target.value !== undefined) {
            this.setState({endDate: event.target.value})
        }
    };

    setFilterCategory = (event) => {
        this.setState({filterCategory: event.target.value})
    };

    clearFilter = (event) => {
        this.setState({startDate: '', endDate: '', filterCategory: "(All)"}, this.loadNotes);
    };

    filter = (event) => {
        const startDate = moment(this.state.startDate);
        const endDate = moment(this.state.endDate);
        if(startDate.isAfter(endDate))
        {
            alert("Start date of filter is after end date!")
            return;
        }

        this.loadNotes();
    };

    editNote(title) {

    }

    deleteNote = (id) => {
        Axios.delete(`${BACKEND}/${id}`).then(res => {
            if(res.status !== 200) {
                alert(res.data);
            }
            else {
                this.loadNotes();
            }
        }).catch(err => {
            alert(err);
        })
    };

    addNote(){

    }

    previousPage = () => {
        this.setState({currentPage: this.state.currentPage-1}, this.loadNotes);
    };

    nextPage = () => {
        this.setState({currentPage: this.state.currentPage - 1 + 2}, this.loadNotes);
    };

    loadNotes = () => {
        Axios.get(`${BACKEND}/notes?startDate=${this.state.startDate}&endDate=${this.state.endDate}&category=${this.state.filterCategory}&page=${this.state.currentPage}`).then(res => {
            this.setState({
                notes: res.data.notes, //data.data?
                categories: res.data.categories,
                currentPage: res.data.currentPage,
                pagesCount: res.data.pagesCount
            })

             sessionStorage.setItem('startDate', this.state.startDate);
             sessionStorage.setItem('endDate', this.state.endDate);
             sessionStorage.setItem('filterCategory', this.state.filterCategory);
             sessionStorage.setItem('page', this.state.currentPage);
        })
    };

    render() {
        return (
            <div className = "container mt-4">
                <div className="row">
                    <div className="col-md-3">
                        <span className="px-2">From:</span>
                        <input className="form-control" type="date" value={this.state.startDate} onChange={this.setStartDate}/>
                    </div>
                    <div className="col-md-3">
                        <span className="px-2">To:</span>
                        <input className="form-control" type="date" value={this.state.endDate} onChange={this.setEndDate}/>
                    </div>
                    <div className="col-md-3">
                        <span>Category:</span>
                        <select name="filterCategory" className="form-control" value={this.state.filterCategory} onChange={this.setFilterCategory}>
                            <option>(All)</option>
                            {
                                this.state.categories.map(cat => {
                                    return <option key={cat} value={cat}>{cat}</option>
                                })
                            }
                        </select>
                    </div>
                    <div className="col-md-3" style={{marginTop: 25}}>
                        <button type="button" onClick={() => this.filter()}>Filter</button>
                        <button type="button" className="btn-primary" onClick={() => this.clearFilter()} style={{marginLeft: 10}}>Clear</button>
                    </div>
                </div>
                <table className="table table-hover" style={{marginTop: 20}}>
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>{
                        this.state.notes.map((note) => {
                            return (
                                <tr key={note.title}>
                                    <td>{note.title}</td>
                                    <td>{note.date}</td>
                                    <td>
                                        <Link to={`edit/${note.title}`}>
                                            <button type="button" className="btn-secondary" onClick={() => this.editNote(note.title)}>Edit</button>
                                        </Link>
                                        <button type="button" className="btn-danger" onClick={() => this.deleteNote(note.id)}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
                <div className={"container mt-4"}>
                    <div className={"row"}>
                        <div className={"col-md-4"}>
                            <Link to={'edit'}>
                                <button type="button" className="btn btn-primary" onClick={() => this.addNote()}>Add Note</button>
                            </Link>
                        </div>
                        <div className={"col-md-4"}>
                            <button type="button" disabled={this.state.currentPage<2} onClick={() => this.previousPage()}>Prev</button>
                            <span>{this.state.currentPage}/{this.state.pagesCount}</span>
                            <button type="button" disabled={this.state.currentPage>=this.state.pagesCount} onClick={() => this.nextPage()}>Next</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NotesList
